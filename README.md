# logging-aop
## Description
This is an example project that logs method parameters on methods with the Loggable annotation.

## Tools
* [AspectJ](http://www.eclipse.org/aspectj/) and [aspectj-maven-plugin](http://www.mojohaus.org/aspectj-maven-plugin/)
* [commons-logging](https://commons.apache.org/proper/commons-logging/)

## Build and run the example
### Run
`mvn clean compile exec:java -Dexec.mainClass="se.kodaren.logging.App"`
### Output
`INFO: arg1=hello`  
`INFO: arg2=world`

package se.kodaren.logging.aop;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

@Aspect
public class LoggingAspect {
    @Around("execution(* *(..)) && @annotation(Loggable)")
    public Object around(ProceedingJoinPoint point) {
        try {
            MethodSignature signature = (MethodSignature)point.getSignature();
            Log log = LogFactory.getLog(this.getClass().getName() + "." + signature.getName());

            Object[] args = point.getArgs();
            String[] params = signature.getParameterNames();
            if(args.length == params.length) {
                for (int i=0; i<args.length; i++) {
                    log.info(params[i] + "=" + args[i]);
                }
            }

            return point.proceed();
        }
        catch (Throwable e) {
            throw new RuntimeException();
        }
    }
}

package se.kodaren.logging;

import se.kodaren.logging.aop.Loggable;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        App app = new App();
        app.doSomething("hello", "world");
    }

    @Loggable
    private void doSomething(String arg1, String arg2) {
        // Do something
    }
}
